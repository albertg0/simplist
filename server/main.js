import { Meteor } from 'meteor/meteor';
var querystring = require('querystring');
import { HTTP } from 'meteor/http';

import '../imports/api/user.js';
Meteor.publish( 'User', () => {
  return User.find();
});
var SpotifyWebApi = require('spotify-web-api-node')
var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};  
var scopes = ['playlist-read-private', 'user-follow-read','user-read-private','user-read-email'];
var redirectUri = 'http://localhost:3000/login',
    clientId = 'cd39e806f0d74c85ba8224c7dc39be0a';
 
var state = generateRandomString(16);
var spotifyApi = new SpotifyWebApi({
  redirectUri : redirectUri,
  clientId : clientId,
  clientSecret: '0b01ccebcd5d449182c6f7429ec40949'
});


Meteor.methods({
   
    spotifyLogin: function(authURL){
        var authorizeURL = spotifyApi.createAuthorizeURL(scopes,state);
     
    },
      removeAllPosts: function() {

        return User.remove({});

      },
    userSearch : function(q){
        //console.log(q);
        // spotifyApi.getMe()
        // .then(function(data){
        //     console.log(data);
        // });
         
      spotifyApi.getUser(q)
      .then(function(data){
         // console.log(data);
          var f = (data.body.id);
          console.log("Data::",f);
          User.insert({name : f});
 
          console.log("User in Db",User.find().fetch());
          return f;
      },function(err){
          console.log(err);
      });
     
      
    },
    getMe:function(code){

    
        console.log('server call',code);

          spotifyApi.authorizationCodeGrant(code)
         .then(function(data){
            console.log("::Code Grant::")
            console.log(data.body);
            var access_token = data.body['access_token'];
            var refresh_token= data.body['refresh_token'];
            
            console.log("Access_Token:: ",access_token);
            spotifyApi.setAccessToken(access_token);
            spotifyApi.setRefreshToken(refresh_token);
         
         });
        
     
        //     spotifyApi.getMe()
        //    .then(function(data) {
        //         if(data)
        //         console.log(data.body);  
        //         return data.body;
        //     },
        //     function(err){
        //         return err;
        //     });
        
       
        
    }
    
});

Meteor.startup(() => {
    /////


    
});
