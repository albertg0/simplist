//import { Mongo } from 'meteor/mongo';

 User = new Mongo.Collection('user');
 
 User.allow({
     
     insert(){
         return true;
     },
     remove(){
         return true;
     }
 });
 
 User.deny({
     
     insert(){
         return false;
     },
     remove(){
         return false;
     }
 });