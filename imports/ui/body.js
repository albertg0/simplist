import { Template } from 'meteor/templating';
import './body.html';

import '../api/user.js';
Meteor.subscribe('User');

var SpotifyWebApi = require('spotify-web-api-node')
var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};  

var scopes = ['playlist-read-private', 'user-follow-read','user-read-private','user-read-email'];
var redirectUri = 'http://localhost:3000/login',
    clientId = 'cd39e806f0d74c85ba8224c7dc39be0a';
 
var state = generateRandomString(16);
var spotifyApi = new SpotifyWebApi({
  redirectUri : redirectUri,
  clientId : clientId,
  clientSecret: '0b01ccebcd5d449182c6f7429ec40949'
});

Template.login.events({
    
    'mousedown #spotifyLogin' : function(event){ 
       // event.preventDefault();
        var authorizeURL = spotifyApi.createAuthorizeURL(scopes,state);
        
        
//        Meteor.call('spotifyLogin',authorizeURL,function(err,data){
//            if(data){
//              
//                Session.set('sHTML',data);
//                Router.go('callback');
//            }
//            if(err)
//            {    console.log(err);
//                 return err;
//            }
//        });
        
        //console.log(authorizeURL);
        window.open(authorizeURL);
        var code = Router.current().params.query.code; 
        console.log(code);  
        Meteor.call('getMe',code,function(err,res){
           if(err)
               return err;
            console.log(err,res); 
        }); 
        
        
    },  

    'click #getMe' : function(req,res){ 
    
    //    var code = Router.current().params.query.code;   
    //     Meteor.call('getMe',code,function(err,res){
    //        if(err)
    //            return err;
    //         console.log(err,res); 
    //     }); 
        Meteor.call('removeAllPosts');
        
    
    },
    
    'mousedown #searchButton' : function(event){
       // event.preventDefault();
        
       
        var f = $('#searchInput').val();
        var q = f;
       // console.log(q);
        Meteor.call("userSearch",q, function(err,res){
            if(err)
                return err;
            console.log(err,res);
            console.log(User.find().fetch());
            
        })
      return false;
    }
    
});

Router.route('login',{
    name:'name',
    template:'login',
    
});

Router.route('callback',{
    name:'callback',
    template:'callback',
    
});
Router.route('/',{
    template:'login'
});

Router.route('home',{
    name:'home',
    template:'home'
});
Router.route('user',
{
    name:'user',
    template:'user'
});

Template.user.helpers({
    
});
Template.login.helpers({
    friends:function(){
        
        console.log(User.find().fetch());
        
        return User.find().fetch();
    }
});





